const express = require('express');
const { check, validationResult } = require('express-validator');

const router = express.Router();
const doneRoute = require('./done');

const validations = [
  check('task')
    .trim()
    .isLength({ min: 3 })
    .escape()
    .withMessage('Minimal length for task name is 3 letter!'),
];

module.exports = (params) => {
  const { tasksService } = params;

  router.get('/', async (req, resp, next) => {
    try {
      const tasks = await tasksService.getList();
      const tasksTodo = tasks.filter((task) => !task.isDone);

      const errors = req.session.tasks ? req.session.tasks.errors : false;
      const successMessage = req.session.tasks ? req.session.tasks.message : false;
      req.session.tasks = {};

      return resp.render('layout', {
        pageTitle: 'Todo List',
        template: 'index',
        tasks: tasksTodo,
        errors,
        successMessage,
        isScript: true,
      });
    } catch (error) {
      return next(error);
    }
  });

  router.post('/', validations, async (req, resp, next) => {
    try {
      const { task } = req.body;
      // handling errors
      const tasks = await tasksService.getList();
      const errors = validationResult(req);
      const taskDuplicate = tasks.find((t) => t.taskName === task);
      if (taskDuplicate) {
        errors.errors.push({
          value: task,
          msg: `Task ${task} already exists!`,
          param: 'task',
          location: 'body',
        });
      }
      if (!errors.isEmpty()) {
        req.session.tasks = {
          errors: errors.array(),
        };
        return resp.redirect('/');
      }
      //

      await tasksService.addEntry(task);
      req.session.tasks = {
        message: 'Task was added!',
      };

      return resp.redirect('/');
    } catch (error) {
      return next(error);
    }
  });

  router.post('/api', validations, async (req, resp, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return resp.json({ errors: errors.array() });
      }
      const { taskName } = req.body;
      await tasksService.addEntry(taskName);
      const tasks = await tasksService.getList();
      return resp.json({ tasks, successMessage: 'Task was added!' });
    } catch (error) {
      return next(error);
    }
  });

  router.post('/api/tasks/:taskName/done', async (req, resp, next) => {
    try {
      const { taskName } = req.params;

      const response = await tasksService.checkStatus(taskName);
      await tasksService.changeStatus(taskName);

      return resp.json({ response });
    } catch (error) {
      return next(error);
    }
  });

  router.use('/done', doneRoute({ tasksService }));

  return router;
};
