const express = require('express');

const router = express.Router();

module.exports = (params) => {
  const { tasksService } = params;

  router.get('/', async (req, resp, next) => {
    try {
      const tasks = await tasksService.getList();
      const tasksDone = tasks.filter((task) => task.isDone);

      const errors = req.session.tasks ? req.session.tasks.errors : false;
      const successMessage = req.session.tasks ? req.session.tasks.message : false;
      req.session.tasks = {};

      return resp.render('layout', {
        pageTitle: 'Done List',
        template: 'done',
        tasks: tasksDone,
        errors,
        successMessage,
        isScript: false,
      });
    } catch (error) {
      return next(error);
    }
  });

  return router;
};
