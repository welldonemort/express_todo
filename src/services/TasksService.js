const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

/**
 * Logic for reading and writing tasks data
 */
class TasksService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSOn file that contains the tasks data
   */
  constructor(datafile) {
    this.datafile = datafile;
  }

  /**
   * Get all tasks items
   */
  async getList() {
    const data = await this.getData();
    return data;
  }

  /**
   * Add a new task item
   * @param {*} taskName The title of the task
   */
  async addEntry(taskName) {
    const data = (await this.getData()) || [];
    data.push({ taskName, isDone: false });
    return writeFile(this.datafile, JSON.stringify(data));
  }

  /**
   * Get task status
   * @param {*} task The title of the task
   */
  async checkStatus(taskName) {
    const data = (await this.getData()) || [];
    const taskFound = data.find((task) => task.taskName === taskName);

    return taskFound ? taskFound.isDone : false;
  }

  /**
   * Get task status
   * @param {*} task The title of the task
   */
  async changeStatus(taskName) {
    const data = (await this.getData()) || [];
    const taskFound = data.find((task) => task.taskName === taskName);

    if (taskFound) {
      taskFound.isDone = !taskFound.isDone;
    }

    return writeFile(this.datafile, JSON.stringify(data));
  }

  /**
   * Fetches tasks data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, 'utf8');
    if (!data) return [];
    return JSON.parse(data);
  }
}

module.exports = TasksService;
