const express = require('express');
const path = require('path');
const makeStoppable = require('stoppable');
const cookieSession = require('cookie-session');
const createError = require('http-errors');
const http = require('http');
const bodyParser = require('body-parser');

const app = express();
const routes = require('./routes');
const TasksService = require('./services/TasksService');

const tasksService = new TasksService(path.join(__dirname, './data/tasks.json'));

app.set('trust proxy', 1);
app.use(
  cookieSession({
    name: 'session',
    keys: ['SAmdaask442!', 'slSLDkdsakd13'],
  })
);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../views'));

app.locals.siteName = 'Simple TODO Application';

app.use(express.static(path.join(__dirname, '../assets')));

app.use(
  '/',
  routes({
    tasksService,
  })
);

app.use((_, resp, next) => next(createError(404, 'Page is not found')));

app.use((err, req, resp, next) => {
  resp.locals.message = err.message;
  const status = err.status || 500;
  resp.locals.status = status;
  resp.status(status);
  resp.render('error');
});

const server = makeStoppable(http.createServer(app));

module.exports = () => {
  const stopServer = () =>
    new Promise((resolve) => {
      server.stop(resolve);
    });

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
};
